import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '../auth/guards/auth.guard';
import {ProjectsCompletedComponent} from './components/projects-completed/projects-completed.component';
import {ProjectsCurrentsComponent} from './components/projects-currents/projects-currents.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectsCurrentsComponent,
    canActivate: [
      AuthGuard
    ],
    children: []
  },
  {
    path: 'current',
    component: ProjectsCurrentsComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'completed',
    component: ProjectsCompletedComponent,
    canActivate: [
      AuthGuard
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
