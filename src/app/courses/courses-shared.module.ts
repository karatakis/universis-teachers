import { NgModule } from '@angular/core';
import {environment} from '../../environments/environment';
import { ModalModule} from 'ngx-bootstrap';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {SharedModule} from '../shared/shared.module';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {CoursesSharedService} from './services/courses-shared.service';
import {CoursesService} from './services/courses.service';
import {GradePipe, GradeScaleService} from './services/grade-scale.service';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        TranslateModule,
        ModalModule.forRoot()
    ],
    providers: [
        CoursesSharedService,
        CoursesService,
        GradeScaleService
    ],
    declarations: [
        GradePipe
    ],
    exports: [
        GradePipe
    ]
})
export class CoursesSharedModule {
    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/courses.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
