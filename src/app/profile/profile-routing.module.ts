import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';
import { ProfileHomeComponent } from './components/profile-home/profile-home.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {
}
