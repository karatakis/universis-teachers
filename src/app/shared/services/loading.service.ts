import { Injectable, RendererFactory2, ComponentFactoryResolver} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SpinnerComponent } from '../modals/spinner.component';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private modalRef: BsModalRef;
  isEnabled: boolean;

  config = {
    ignoreBackdropClick: true,
    animated: false,
    keyboard: false,
    class: 'modal-dialog-centered'
  };

  constructor(private modalService: BsModalService) {
  }

  toggle() {
    if (this.isEnabled) {
      this.hideLoading();
    } else {
      this.showLoading();
    }
  }

  showLoading() {
    if (!this.isEnabled) {
      this.modalRef = this.modalService.show(SpinnerComponent, this.config);
      this.isEnabled = true;
    }
  }

  hideLoading() {
    if (this.isEnabled) {
      // delay hiding loader for preventing screen flickering
      setTimeout(() => {
        this.modalRef.hide();
        this.isEnabled = false;
      }, 500);
    }
  }
}
